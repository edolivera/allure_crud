<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Contact;
use Redirect;
use DB;

class MainController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
    	$dados = [
    		'page' => 'principal'
    	];

    	return view ("template", $dados);
    }

    public function usuarios_listar()
    {
    	$users = DB::table('contacts') ->paginate(10);
                                        
        $dados = [
    		'page'    => 'usuarios',
            'users'   =>  $users,
    	];

    	return view ("template", $dados);
    }

    public function usuario_cadastrar(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);

        $contact = new Contact([
            'name' => $request->post('name'),
            'email' => $request->post('email'),
            'password' => $request->post('password')
        ]);
        $contact->save();
        
        $dados = [
            'page' => 'usuarios',
            'user' =>$contact
        ];

        return redirect('/lista-de-usuarios')->with('success', 'Usuário criado com sucesso!!!' );  ;
    }

    public function usuario_editar($id, Request $request)
    {  
        $user                   = Contact::findOrFail($id);
        $user                   ->update($request->all());
        //$users                  = Contact::all();
        $users = DB::table('contacts') ->paginate(10);
        //$user_id                = Auth::user()->id;
        $usuario                = Contact::where('id', '=', $id)->get();
      //  $aviso_feedback         =  Feedback::aviso_feedback($user_id);

        $dados=[
            'page'              =>  'usuarios',
            'users'             =>  $users,
            'user'              =>  $user,
            'usuario'           =>  $usuario,
         //   'aviso_feedback'    =>$aviso_feedback,
        ];
        return view ('template', $dados); 
    }   

    public function usuario_atualizar($id, Request $request)
    {
        $user = Contact::findOrFail($id);

        if($request['status']==1){
            $email_verified_at = now();
        }else{
            $email_verified_at = null;
        }
        $name = $request['name'];
        $dados=[
            'name'                  => $request['name'],
            'email'                 => $request['email'],
            'password'              => $request['password'],
            'email_verified_at' => $email_verified_at,
        ];

        $user->update($dados);
        return Redirect::to($id.'/editar-usuario')->with('return', 'Dados atualizados com sucesso, '.$name.'!!!' ); 
    }

    public function usuario_confirmar_deletar($id)
    {
        \Session::flash('danger', 'Atenção: Você está prestes a excluir este usuario, esta ação não pode ser desfeita!!!
            Por favor, clique em: SIM, para confirmar, ou acesse outra seção do site');
        
        $users                  = DB::table('contacts') ->paginate(10);
      //  $usuario                = Contact::findOrFail($id);
        $usuario                = DB::table('contacts')
                                    ->Where('id',$id)
                                    ->get();


        $dados=[
            'page'              =>  'usuarios',
            'users'             =>  $users,
            'usuario'           =>  $usuario,
        ];
        return view ('template', $dados);

        
    }

    public function usuario_deletar($id)
    {
        $usuario                = Contact::findOrFail($id);   
        $usuario                ->delete();
        \Session::flash('return', 'Usuario excluído com Sucesso');

       return Redirect::to('lista-de-usuarios')->with('return', 'Usuário deletado com sucesso!!!'); 
    }

 public function resultado_pesquisa (Request $request)
    {
        $campo_de_pesquisa = $request->campo_de_pesquisa;

        $resultado = DB::table('contacts')
                    ->where('name', 'like', '%'.$campo_de_pesquisa.'%')
                    ->orderBy('id', 'asc')
                    ->paginate(10);

        $quantidade_de_resultados = DB::table('contacts')
                                    ->where('name', 'like', '%'.$campo_de_pesquisa.'%')
                                    ->count();  

            $dados = [
                'page'                      => 'resultado_pesquisa',
                'resultado'                 => $resultado,
                'campo_de_pesquisa'         => $campo_de_pesquisa,
                'quantidade_de_resultados'  => $quantidade_de_resultados 
            ];

        return view('template', $dados);        
    }


} //fechamento inicial
