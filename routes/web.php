<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/resultado-pesquisa', 'MainController@resultado_pesquisa');
Route::post('/resultado-pesquisa', 'MainController@resultado_pesquisa');
Route::get('/lista-de-usuarios', 'MainController@usuarios_listar');
Route::post('/cadastrar-usuario', 'MainController@usuario_cadastrar');
Route::get('/{id}/editar-usuario', 'MainController@usuario_editar');
Route::post('/{id}/editar-usuario', 'MainController@usuario_editar');
Route::patch('/{id}/atualizar-usuario', 'MainController@usuario_atualizar');
Route::post('/{id}/confirmar-deletar-usuario', 'MainController@usuario_confirmar_deletar');
Route::get('/{id}/confirmar-deletar-usuario', 'MainController@usuario_confirmar_deletar');
Route::delete('/{id}/deletar-usuario', 'MainController@usuario_deletar');