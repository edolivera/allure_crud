Em um prompt de comando
 de sua preferência, 
 copie e cole:		-> composer install
 					-> php -r "copy('.env.example', '.env');"
					-> php artisan key:generate


Crie um banco de dados
MySQL, chamado "db"
Na raiz da pasta: "allure_crud"
Há o arquivo: "db.sql"
E importe seus dados
para o banco recém criado.


Em qualquer editor de texto,
configurar arquivo ".env"
sem aspas mesmo, 
- com as opções:	-> DB_DATABASE=db
					-> DB_USERNAME=allure
					-> DB_PASSWORD=allurecom


O ".env", está na raiz
da pasta "allure_crud"
Pode deixar as outras opções
como estão. 


A seguir, 
no prompt, digite:	-> php artisan migrate		
					-> php artisan serve

E
no browser, acesse : http://localhost:8000

O menu: "usuários" possui as opções
para cadastro, edição e remoção
de usuários.As demais seções são
apenas parte do tema e 
não têm funcionalidade.