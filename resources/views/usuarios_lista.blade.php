<!---Lista de usuários-->
	@if($users !="")
  <h1>Lista de Usuários</h1>
     <div class="margem_cima_baixo">
         <!-- @if(count($users)>=1) -->    
          <h3>Usuários Cadastrados</h3> {{ $users->links() }}
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th class="col">Nome</th>
                  <th class="col">E-mail</th>
                  <th class="col">Senha</th>
                  <th class="col"></th>
                  <th class="col"></th>
                </tr>
              </thead>
              <tbody>
                  @foreach($users as $users)
                  <tr>
                    <td class="col">{{ $users->name }}</td>
                    <td class="col">{{ $users->email }}</td>
                    <td class="col">{{ $users->password }}</td>
                    <th>
                    {!! Form::open(['method' =>'POST', 'url' =>$users->id.'/editar-usuario'])!!}
                    {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    </th>
                    <th>
                    {!! Form::open(['method' =>'POST', 'url' =>$users->id.'/confirmar-deletar-usuario'])!!}
                    {!! Form::submit('Deletar', ['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    </th>                   
                  </tr>
                  @endforeach
              </tbody>
            </table>

          <!-- @endif -->
          </div>
          </div>
        @endif 
      <!--fim da lista de usuarios-->