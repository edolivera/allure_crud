
@if($quantidade_de_resultados >1) {{ $resultado->links() }}

   <h3> {{$quantidade_de_resultados}} Usuários encontrados</h3>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th class="col">Nome</th>
                   <th class="col">E-mail</th>
                   <th class="col">Senha</th>
                    <th></th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                  @foreach($resultado as $users)
                  <tr>
                    <td>{{ $users->id }}</td>
                    <td>{{ $users->name }}</td>
                    <td>{{ $users->email }}</td>
                    <td>{{ $users->password }}</td>
                    <th>
                    {!! Form::open(['method' =>'POST', 'url' =>$users->id.'/editar-usuario'])!!}
                    {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    </th>
                    <th>
                    {!! Form::open(['method' =>'POST', 'url' =>$users->id.'/confirmar-deletar-usuario'])!!}
                    {!! Form::submit('Deletar', ['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    </th>                   
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>


@elseif($quantidade_de_resultados ==1)

   <h3> {{$quantidade_de_resultados}} Usuário encontrado</h3>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th class="col">Nome</th>
                   <th class="col">E-mail</th>
                   <th class="col">Senha</th>
                    <th></th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                  @foreach($resultado as $users)
                  <tr>
                    <td>{{ $users->id }}</td>
                    <td>{{ $users->name }}</td>
                    <td>{{ $users->email }}</td>
                    <td>{{ $users->password }}</td>
                    <th>
                    {!! Form::open(['method' =>'POST', 'url' =>$users->id.'/editar-usuario'])!!}
                    {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    </th>
                    <th>
                    {!! Form::open(['method' =>'POST', 'url' =>$users->id.'/confirmar-deletar-usuario'])!!}
                    {!! Form::submit('Deletar', ['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    </th>                   
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>


@else
    <h3><span>Nenhum resultado encontrado, por favor, realize uma nova pesquisa com outro termo</span></h3> 
@endif