
    @if(Request::is('*/editar-usuario'))

          @if(Session::has('return'))
            <div class="alert alert-success">{{Session::get('return') }}
            </div>
          @endif
          @include('usuario_editar');
                

    @elseif(Request::is('*/confirmar-deletar-usuario'))          
          
          @if(Session::has('danger'))
            <div class="alert alert-danger">{{Session::get('danger') }}
            </div>
          @endif    

          @include('usuario_deletar');
    @else


@if(Session::has('return'))
  <div class="alert alert-success">{{Session::get('return') }}
  </div>
@endif



@include('usuario_cadastrar');



    @endif
      <!-- Content Row -->


@include('usuarios_lista');


