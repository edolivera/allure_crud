        <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-9">
                <h3>Editar Usuário </h3>
                  @foreach($usuario as $row)
                    {!! Form::model('$user',['method' => 'PATCH','url' => $row->id.'/atualizar-usuario']) !!}
                    <input type="text" class="form-control" name="name" value="{{ $row->name }}">
                    <input type="email" class="form-control" name="email" value="{{ $row->email }}">
                    <input type="password" class="form-control" name="password" value="{{ $row->password }}">
                    {!! Form::submit('Atualizar', ['class'=>'d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm']) !!}
                    {!! Form::close() !!}
                  @endforeach
            </div>
		    </div>