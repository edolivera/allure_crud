          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-9">
              <button name="cadastrar_usuario" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <h3>Cadastrar Usuário </h3>
              </button>
                <div id="cadastrar_usuario">
                {!! Form::open(['url' => 'cadastrar-usuario']) !!}
                    <div class="form-group">
                      {!! Form::label('name', 'Nome:') !!}
                      {!! Form::input('text', 'name', '', ['class' => 'form-control', 'placeholder' =>'Nome', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('email', 'E-mail:') !!}
                      {!! Form::input('email', 'email', '', ['class' => 'form-control', 'placeholder' =>'E-mail', 'required' => 'required']) !!}
                    </div>
                     <div class="form-group">
                        {!! Form::label('password', 'Senha:') !!}
                        {!! Form::input('password', 'password', '', ['class' => 'form-control', 'placeholder' =>'Senha', 'required' => 'required']) !!}
                    </div>
                    <div class=" form-group">
                      {!! Form::submit('Cadastrar', ['class'=>'d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm']) !!}
                      {!! Form::close() !!}
                    </div>
                </div>
            </div>
		    </div>